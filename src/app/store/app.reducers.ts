import { createReducer, on } from "@ngrx/store";
import { cartAddItem, cartRemoveItem, filterProducts, loadCategories, loadCategoriesSuccess, loadProductSuccess, sortProducts } from "./app.actions";
import { AppState } from "./model/app.state";

export const initialAppState: AppState = {
    products: [],
    categories: [],
    cart: [],
    sortProductBy: "Name",
    selectedCategory: 0, // All
    isLoading: false
};

export const AppReducer = createReducer(
    initialAppState,
    on(loadCategories, (state) => { return { ...state, isLoading: true }}),
    on(loadCategoriesSuccess, (state, { categories }) => ({ ...state, categories: categories })),
    on(loadProductSuccess, (state, { products }) => ({ ...state, isLoading: false, products: products })),
    on(filterProducts, (state, { categoryId }) => ({ ...state, selectedCategory: categoryId })),
    on(sortProducts, (state, { sortBy }) => ({ ...state, sortProductBy: sortBy })),
    on(cartAddItem, (state, { product }) => 
    ({ ...state, 
        cart: [...state.cart, product],
        products: state.products.map((item) => {
            if(item.id == product.id) {
                return {...item, addedToCart: true};
            }
            return item;
        })
    })),
    on(cartRemoveItem, (state, { id }) => 
    ({ ...state,
        cart: state.cart.filter((item) => item.id != id),
        products: state.products.map((item) => {
            if(item.id == id) {
                return { ...item, addedToCart: false }
            }
            return item;
        })
    }))
);