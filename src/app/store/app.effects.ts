import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { EMPTY } from "rxjs";
import { catchError, map, mergeMap, switchMap } from "rxjs/operators";
import { CategoryService } from "../services/category.service";
import { ProductService } from "../services/product.service";
import { loadCategoriesSuccess, loadProducts, loadProductSuccess, LOAD_CATEGORIES, LOAD_PRODUCTS } from "./app.actions";
import { Product } from "./model/app.state";

@Injectable()
export class AppEffects {
    constructor(
        private action$: Actions,
        private categoryService: CategoryService,
        private productService: ProductService) { }

    loadCategories$ = createEffect(() => 
        this.action$.pipe(
            ofType(LOAD_CATEGORIES),
            mergeMap(() => this.categoryService.loadCategories().pipe(
                map((categoryList) => {
                    const categories = categoryList.map((category) => ({ ...category, selected: false }))
                    return categories;
                }),
                switchMap((categories) => [
                    loadCategoriesSuccess({ categories }),
                    loadProducts()
                ]),
                catchError(() => EMPTY))
            )
        )
    );

    loadProducts$ = createEffect(() => 
        this.action$.pipe(
            ofType(LOAD_PRODUCTS),
            mergeMap(() => this.productService.loadProducts().pipe(
                map((productList) => {
                    const products = productList.map((product) => ({ ...product, addedToCart: false}));
                    return loadProductSuccess({ products });
                }))
            )
        )
    );
}