import { createFeatureSelector, createSelector } from "@ngrx/store";
import { AppState } from "./model/app.state";

export const selectAppState = createFeatureSelector<AppState>('appState');

export const selectedCategory = createSelector(selectAppState, (state) => state.selectedCategory);
export const selectCategories = createSelector(
    selectAppState, 
    selectedCategory, 
    (state, selectedCategory) => 
        state.categories.map((category) => ({ ...category, selected: category.id == selectedCategory }))
);

export const selectSortBy = createSelector(selectAppState, (state) => state.sortProductBy);
export const selectProducts = createSelector(
    selectAppState, 
    selectedCategory,
    selectSortBy,
    (state, categoryId, sortBy) => {
        const products = state.products.filter((product) => product.categoryId == categoryId || 0 == categoryId);
        if(sortBy == "Price") {
            products.sort((a, b) => a.price - b.price);
        }
        else {
            products.sort((a, b) => {
                if(a.productName.toLowerCase() < b.productName.toLowerCase()) {
                    return -1;
                }
                if(a.productName.toLowerCase() > b.productName.toLowerCase()) {
                    return 1;
                }
                return 0;
            });
        }
        return products;
    }
);

export const selectCart = createSelector(selectAppState, (state) => state.cart);
export const selectCartItemCount = createSelector(selectCart, (cart) => cart.length);
export const selectCartTotalAmount = createSelector(selectCart, (cart) => cart.map((item) => item.price).reduce((a, b) => a + b, 0))