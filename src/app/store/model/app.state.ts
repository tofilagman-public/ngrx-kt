export interface Product {
    id: number;
    productName: string;
    productUnit: string;
    price: number;
    categoryId: number;
    addedToCart: boolean;
}

export interface Category {
    id: number;
    categoryName: string;
    selected: boolean;
}

export interface AppState {
    products: ReadonlyArray<Product> | [];
    categories: ReadonlyArray<Category> | [];
    cart: ReadonlyArray<Product> | [];
    sortProductBy: string;
    selectedCategory: number;
    isLoading: boolean | null;
}