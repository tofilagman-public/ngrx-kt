import { createAction, props } from "@ngrx/store";
import { Category, Product } from "./model/app.state";

// Categories
export const LOAD_CATEGORIES = "[Categories] Load Categories";
export const LOAD_CATEGORIES_SUCCESS = "[Categories] Load Categories Success";

export const loadCategories = createAction(
    LOAD_CATEGORIES
);

export const loadCategoriesSuccess = createAction(
    LOAD_CATEGORIES_SUCCESS,
    props<{ categories: Category[] }>()
);

// Product
export const LOAD_PRODUCTS = "[Product] Load Products";
export const LOAD_PRODUCTS_SUCCESS = "[Product] Load Products Success";

export const loadProducts = createAction(
    LOAD_PRODUCTS
);

export const loadProductSuccess = createAction(
    LOAD_PRODUCTS_SUCCESS,
    props<{ products: Product[] }>()
);

// Filter
export const FILTER_PRODUCTS = "[Filter] Filter Products";
export const filterProducts = createAction(
    FILTER_PRODUCTS,
    props<{ categoryId: number }>()
);

// Sort
export const SORT_PRODUCTS = "[Sort] Sort Products";
export const sortProducts = createAction(
    SORT_PRODUCTS,
    props<{ sortBy: string }>()
);

// Cart
export const CART_ADD_ITEM = "[Cart] Add Item";
export const CART_REMOVE_ITEM = "[Cart] Remove Item";

export const cartAddItem = createAction(
    CART_ADD_ITEM,
    props<{ product: Product }>()
);

export const cartRemoveItem = createAction(
    CART_REMOVE_ITEM,
    props<{ id: number }>()
);