import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectCart, selectCartItemCount, selectCartTotalAmount } from '../store/app.selectors';
import { Product } from '../store/model/app.state';

@Component({
  selector: 'app-cart-widget',
  templateUrl: './cart-widget.component.html',
  styleUrls: ['./cart-widget.component.css']
})
export class CartWidgetComponent implements OnInit {
  public cart$: Observable<ReadonlyArray<Product>> | null = null;
  public totalAmount$: Observable<number> | null = null;
  public cartItemCount$: Observable<number> | null = null;

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.cart$ = this.store.select(selectCart);
    this.totalAmount$ = this.store.select(selectCartTotalAmount);
    this.cartItemCount$ = this.store.select(selectCartItemCount);
  }

}
