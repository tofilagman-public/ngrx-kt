export interface Product {
    id: number;
    productName: string;
    productUnit: string;
    price: number;
    categoryId: number;
}