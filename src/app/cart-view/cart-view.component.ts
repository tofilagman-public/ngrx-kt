import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { cartRemoveItem } from '../store/app.actions';
import { selectCart, selectCartTotalAmount } from '../store/app.selectors';
import { Product } from '../store/model/app.state';

@Component({
  selector: 'app-cart-view',
  templateUrl: './cart-view.component.html',
  styles: [
  ]
})
export class CartViewComponent implements OnInit {
  public cart$: Observable<ReadonlyArray<Product>> | null = null;
  public cartTotalAmount$: Observable<number> | null = null;

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.cart$ = this.store.select(selectCart);
    this.cartTotalAmount$ = this.store.select(selectCartTotalAmount);
  }

  remove(itemId: number) {
    this.store.dispatch(cartRemoveItem({ id: itemId }));
  }
}
