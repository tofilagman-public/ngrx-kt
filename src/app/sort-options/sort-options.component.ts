import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { sortProducts } from '../store/app.actions';
import { selectSortBy } from '../store/app.selectors';

@Component({
  selector: 'app-sort-options',
  templateUrl: './sort-options.component.html',
  styleUrls: ['./sort-options.component.css']
})
export class SortOptionsComponent implements OnInit {
  public sortBy$: Observable<string> | null = null;

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.sortBy$ = this.store.select(selectSortBy);
  }

  onClick(sortBy: string) {
    this.store.dispatch(sortProducts({ sortBy }));
  }
}
